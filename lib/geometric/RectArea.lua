require("lib.OOP")
--## package path
packagePath("lib.geometric.RectArea")
--## package requirements
local Point=require("lib.geometric.Point")
--## class: RectArea
--我代表一個方形的區域，我知道我的大小、位置、頂點與中心點。
--RectArea 物件被建立之後，各項屬性會被自動計算並填入。但若在建立後手動修改屬性，並不會使其他屬性同步更新。
local RectArea=class("RectArea",Point)--建立新類別
--## static private property

--## static public property

--## static private method

--## static public method
--建立實例(物件)
func.new{Number,Number,Number,Number,
function (x,y,width,height)
	local rectArea=object(RectArea,Point:new(x,y))--建立新物件
	--## instance private property
	
	--## instance public property
	rectArea.x=x
	rectArea.y=y
	rectArea.width=width
	rectArea.height=height
	rectArea.left=x
	rectArea.right=x+width
	rectArea.centerX=x+width*.5
	rectArea.top=y
	rectArea.bottom=y+height
	rectArea.centerY=y+height*.5
	--## instance private method
	
	--## instance public method
	--檢驗指定的座標點是否在矩形區域當中
	func.pointInside{Point,
	function(point)
		local result=(point.x>=rectArea.left and point.x<=rectArea.right) and (point.y>=rectArea.top and point.y<=rectArea.bottom)
		return result
	end,
	Boolean}
	--傳回物件
	return endObject()
end}

--傳回類別
return endClass()

