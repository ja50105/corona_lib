--## package path
packagePath("lib.qll.net.QllS3Bucket")
--## package requirements
local OOP=require("lib.OOP")
local S3=require("lib.s3.s3")
--## class: QllS3Bucket
--QllS3Bucket 類別能協助簡化對s3的檔案操作流程。
--*注意：s3 並沒有資料夾的概念，檔案被視為資料物件，檔案名稱則被視為存取物件的索引。檔案名稱可接受"/"字元，因此仍然可以模擬檔案目錄的架構。
--若透過 remoteDir 屬性指定遠端資料夾路徑，則 QllS3Bucket 將自動把路徑附加到檔名前端，來模擬資料夾的結構。
--透過設定 localBaseDir 來決定本地根目錄的位置，預設為 system.DocumentsDirectory。設定 localDir 屬性，來決定子目錄路徑。
local QllS3Bucket=abstract(class("QllS3Bucket"))
--## static private property
local bucket
local _localBaseDir=system.DocumentsDirectory --本地根目錄

--## static public property
QllS3Bucket.remoteDir="usr_data"--(String) 遠端目錄
QllS3Bucket.localDir=""--(String) 本地目錄
QllS3Bucket.showResult=true--顯示執行結果文字
--## static private method
--初始化
local function init()
	S3.AWS_Access_Key_ID = "AKIAI2MQ6I57H7XPTY5Q"
	S3.AWS_Secret_Key    = "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"
	-- Get a bucket object
	bucket = S3.getBucket("qllco")
end
--顯示結果狀態
local function printStatus(status)
	if(QllS3Bucket.showResult) then
		if(not status.isError) then
			print("status:",status.response.status)
		else
			print("ERROR:",status.errorMessage)
		end
	end
end
--下載完成
local function dLoadComplete(status)
	print("\n===== Download =====")
	printStatus(status)
end
--上傳完成
local function uLoadComplete(status)
	print("\n===== Upload =====")
	printStatus(status)
end
--刪除完成
local function deleteComplete(status)
	print("\n===== Delete =====")
	printStatus(status)
end
--清單已取完成
local function listComplete(status)
	print("\n===== List =====")
	if(not status.isError) then
		print("status:",status.response.status)
		--列舉清單項目
		if(status.bucket.Contents) then
			local contentCount=#status.bucket.Contents
			print("Found " .. contentCount .. " items in bucket")
			for i = 1, contentCount do
				local content=status.bucket.Contents[i]			
				print("Key: "..content.Key)
			end		
		end
	else
		print("ERROR:",status.errorMessage)
	end
	
end
--## static public method
--localBaseDir 設置本地根目錄
--(UserData)
--注意：此屬性的值，不可為字串。請使用 system.DocumentsDirectory, system.ResourceDirectory, system.TemporaryDirectory 等值來填入。
function set:localBaseDir(arg)
	if(type(arg)==UserData) then
		_localBaseDir=arg
	else
		print("ERROR:Setting 'QllS3Bucket.localBaseDir' failed. Input value should be an UserData such as system.DocumentsDirectory.")
	end
end
function get:localBaseDir()
	return _localBaseDir
end

--下載
func.download{String,Nil,1,
function (fileName,requestHandler)
	local remoteFilePath=QllS3Bucket.remoteDir.."/"..fileName
	requestHandler=requestHandler or dLoadComplete
	local localFilePath
	if(QllS3Bucket.localDir) then
		localFilePath=QllS3Bucket.localDir.."\\"..fileName
	else
		localFilePath=fileName
	end
	bucket:get_file(remoteFilePath, system.pathForFile(localFilePath, _localBaseDir),nil,requestHandler)
end}
--上傳
func.upload{String,Nil,
function (fileName,requestHandler)
	local remoteFilePath=QllS3Bucket.remoteDir.."/"..fileName
	requestHandler=requestHandler or uLoadComplete
	local localFilePath
	if(QllS3Bucket.localDir) then
		localFilePath=QllS3Bucket.localDir.."\\"..fileName
	else
		localFilePath=fileName
	end
	bucket:put_file(remoteFilePath, system.pathForFile(localFilePath, _localBaseDir),nil,requestHandler)
end}
--刪除
func.delete{String,Nil,
function (fileName,requestHandler)
	local remoteFilePath=QllS3Bucket.remoteDir.."/"..fileName
	requestHandler=requestHandler or deleteComplete
	bucket:delete(remoteFilePath,requestHandler)
end}
--取得清單
func.list{Nil,
function (requestHandler)
	requestHandler=requestHandler or listComplete
	bucket:list("/", QllS3Bucket.remoteDir.."/", 100,nil,requestHandler)
end}

init()
--傳回類別
return endClass()

