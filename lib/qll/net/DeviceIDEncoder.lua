--[[----------------------------------------
File:		"encode.lua"
Author:		YodaLee	,(Bob)
Description: encode deviceID
Date:		2012-03-09 
Update:		2012-07-16	by:Chris Yang	changement:封裝成類別(Class)
]]------------------------------------------- 
require("lib.OOP")
--## package path
packagePath("lib.qll.net.DeviceIDEncoder")
--## package requirements
--## class: DeviceIDEncoder
--此類別為QLL 專用，在將 DeviceID 傳給伺服器之前，會先使用此類別的 encode()方法，將 DeviceID 編碼。
local DeviceIDEncoder=abstract(class("DeviceIDEncoder"))
--## static public method
--編碼
--deviceID (string) Device ID
--傳回值 (string) 編碼後的字串
func.encode{String,
function( deviceID )
  local xcode=deviceID
  local xtmp  = '';
  local xtmp1 = '';
  local xtmp2 = '';
  
  for i=1 ,string.len(xcode),2 do
    xtmp = xtmp .. string.sub(xcode,i,i);
  end
  
  -- print(xtmp,"\n");
  
  for i=1 ,string.len(xtmp) do
    xtmp1 = xtmp1 .. string.sub(xtmp,i+1,i+1);
  end
  xtmp1 = xtmp1 .. string.sub(xtmp,1,1);
  
  for i=1 ,string.len(xcode) do
    if (((i-1) % 2) == 0) then
      xtmp2 = xtmp2 .. string.sub(xtmp1,(i+1)/2,(i+1)/2);
    else
      xtmp2 = xtmp2 .. string.sub(xcode,i,i);
    end
  end
	
	-- print(xtmp1,"\n");
	-- print(xtmp2,"\n");

  return xtmp2;
end,String}

return endClass()