--## package path
packagePath("lib.qll.net.QllCGIRequest")
--## package requirements
local OOP=require("lib.OOP")
local http=require("socket.http")
local DeviceIDEncoder=require("lib.qll.net.DeviceIDEncoder")

--## class: QllCGIRequest
--此類別將QLL 所需使用的CGI 呼叫都集中在此，簡化了呼叫流程與管理眾多不同CGI 的麻煩。只需透過這裡的方法，輸入適當的參數，就可以完成呼叫。此類別也提供了 testMode 屬性，方便CGI的測試。
--*對於那些只在單一APP中一次性使用的CGI，並不建議加入這個類別的方法，請使用 customQllRequest() 或 customRequest() 方法來呼叫CGI。
local QllCGIRequest=abstract(class("QllCGIRequest"))--建立新類別
--## static private property
local URL_PREFIX_WEB="web"
local URL_PREFIX_STAGING="staging"
local urlPrefix=URL_PREFIX_WEB
local _testMode=false
--## static public property
QllCGIRequest.debugPrint=true

--## static private method
--由於原本的 network.request故障無法使用，故以此函數包裝 http.request，以便模擬 network.request 的返回格式。
local function httpRequest(url,requestHandler)
	-- print("URL",url)
	local response=http.request(url)
	local isErrorHtml=response~="" and (type(response)~="string" or string.sub(response, 1,1)=="<")
	requestHandler({isError=isErrorHtml,response=response})
end
--除錯模式印出資訊
local function printURL(url)
	if(QllCGIRequest.debugPrint) then
		print("QllCGIRequest debug url: "..url)
	end
end
--## static public method
--從伺服器取得廣告資訊
--key (String or Number) 可為 FLURRYKEY 或 app ID
--language (String) 語言，可從AppInfo取得
--requestHandler (Function) 在收到request回應時觸發的Listener函數。函數會被傳入一個參數event。包含event.isError 和 event.response 屬性。
--[returnUTF8] (Boolean) 若為true，則以UTF-8 格式傳回資料
func.adsRequest{Nil,String,Function,Nil,
function (key,language,requestHandler,returnUTF8)
	local utf8=""
	if(returnUTF8) then
		utf8="&code=utf8"
	end
	local url="http://"..urlPrefix..".qll.co/api/ads/?key="..key.."&l="..language..utf8
	printURL(url)
	network.request( url, "GET" ,requestHandler)
end}

--從伺服器取得 新的使用者ID 與使用者資訊
--deviceID (String) 裝置ID。
--key (String or Number) 可為 FLURRYKEY 或 app ID
--requestHandler (Function) 在收到request回應時觸發的Listener函數。函數會被傳入一個參數event。包含event.isError 和 event.response 屬性。
func.newQIDRequest{String,Any,String,String,Function,
function (deviceID,key,model,language,requestHandler)
	local encodedDeviceID=DeviceIDEncoder:encode(deviceID)
	local modelString = "model=" .. socket_url.escape(model)
	local localeString = "&locale=".. language
	local url="http://"..urlPrefix..".qll.co/api/init/"..encodedDeviceID.."/"..key.."?"..modelString..localeString
	printURL(url)
	network.request(url , "GET", requestHandler)
end}

--從伺服器取得使用者的更新資訊
--q (Number) 正整數，特定使用者在特定裝置上的特定APP之唯一代號。
--requestHandler (Function) 在收到request回應時觸發的Listener函數。函數會被傳入一個參數event。包含event.isError 和 event.response 屬性。
func.qDataRequest{Number,String,String,Function,
function (q,model,language,requestHandler)
	local modelString = "model=" .. socket_url.escape(model)
	local localeString = "&locale=".. language
	local url="http://"..urlPrefix..".qll.co/api/init/"..q.."?"..modelString..localeString
	printURL(url)
	network.request(url, "GET", requestHandler)
end}

--發送註冊訊息給伺服器
--q (Number) 正整數，特定使用者在特定裝置上的特定APP之唯一代號。
--email (String) 使用者用來註冊的e-mail。
--requestHandler (function) 在收到request回應時觸發的Listener函數。函數會被傳入一個參數event。包含event.isError 和 event.response 屬性。
func.registerRequest{Number,String,Function,
function (q,email,requestHandler)
	local url="http://"..urlPrefix..".qll.co/api/register?q="..q.."&email="..email
	printURL(url)
	network.request(url, "GET", requestHandler)
end}

--回報彈出訊息被點擊
--q (Number) 正整數，特定使用者在特定裝置上的特定APP之唯一代號。
--notifyMsgID (Number) 彈出訊息代號
--requestHandler (function) 在收到request回應時觸發的Listener函數。函數會被傳入一個參數event。包含event.isError 和 event.response 屬性。
func.notifyMsgClickedRequest{Number,Number,Function,
function (q,notifyMsgID,requestHandler)
	local url="http://"..urlPrefix..".qll.co/api/adclick?q="..q.."&notifyid="..notifyMsgID
	printURL(url)
	network.request(url, "GET", requestHandler)
end}

--取得 hash的種子，這個種子是一個數字，可以在透過演算法之後產生hash，產生 hash的工作目前由 AppInfo 負責。請參閱 AppInfo的說明。
--q (Number) 正整數，特定使用者在特定裝置上的特定APP之唯一代號。
--requestHandler (function) 在收到request回應時觸發的Listener函數。函數會被傳入一個參數event。包含event.isError 和 event.response 屬性。
func.getHashSeedRequest{Number,Function,
function (q,requestHandler)
	local url="http://web.qll.co/api/utils/get_hash_seed/"..q
	printURL(url)
	network.request(url, "GET", requestHandler)
end}

--自訂QllRequest
--url會自動以 "http://web.qll.co/" 開頭，只需輸入後半段的設定即可。
--對於那些只在單一APP中一次性使用的CGI，只需使用 customQllRequest 方法來達成CGI的呼叫即可。
--command (String) 所要呼叫的 Api 命令，中間可以有"/"。
--argTable (Table) 包含參數的table，例如：{q=10,uid=99}。
--requestHandler (function) 在收到request回應時觸發的Listener函數。函數會被傳入一個參數event。包含event.isError 和 event.response 屬性。
--[useHTTPRequest] (Boolean) 使用同步式的http請求，在Request得到回應之前，後續的程式碼將不會被執行。
--*注意：testMode 屬性，也會影響到此方法。
func.customQllRequest{String,Table,Function,Boolean,3,
function (command,argTable,requestHandler,useHTTPRequest)
	--解析參數
	local argString=""
	for key, value in pairs(argTable) do
		if(argString~="") then
			argString=argString.."&"
		end
		argString=argString..key.."="..value
	end
	if(argString~="") then
		argString="?"..argString
	end
	--發送
	local url="http://"..urlPrefix..".qll.co/"..command..argString
	printURL(url)
	if(useHTTPRequest) then
		httpRequest(url,requestHandler)
	else
		network.request(url, "GET", requestHandler)
	end
end}

--自訂Request
--*注意：testMode 屬性，對此方法 沒有效果！
func.customRequest{String,Function,
function (url,requestHandler)
	httpRequest(url,requestHandler)
end}

--testMode (Boolean) 測試模式
--在除錯模式當中，所發出的 Request，會導向伺服器的模擬測試平台。這樣可以防止真正的用戶資料被變更。
--一旦這個屬性被設定，就會對後續的CGI呼叫產生影響。
function set:testMode(arg)
	_testMode=arg
	if(_testMode) then
		urlPrefix=URL_PREFIX_STAGING
	else
		urlPrefix=URL_PREFIX_WEB
	end
end
function get:testMode(arg)
	return _testMode
end

--傳回類別
return endClass()

