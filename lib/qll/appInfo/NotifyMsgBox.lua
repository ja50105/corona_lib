require("lib.OOP")
--## package path
packagePath("lib.qll.appInfo.NotifyMsgBox")
--## package requirements
local QllCGIRequest=require("lib.qll.net.QllCGIRequest")
--## class: NotifyMsgBox
--QLL 在大部份APP中都會有的「用戶訊息」彈出視窗。正常狀況下 NotifyMsgBox並不需要手動建立，NotifyMsgBox 會由 AppInfo 建立，並且可透過 AppInfo.notifyMsgBox 取得。
--這個彈出視窗的內容，由AppInfo從伺服器取得。
--使用 show()方法來使視窗彈出。
local NotifyMsgBox=class("NotifyMsgBox")--建立新類別，[extendsClass] 繼承的類別
--## static private property

--## static public property

--## static private method

--## static public method
--建立實例(物件)
--當參數id被傳入0的時候，即使呼叫show()方法，也不會彈出對話框。
--AppInfo:請將AppInfo類別初始化後，將它傳入作為此參數
func.new{Class,Number,String,String,
function (AppInfo,id,message,url)
	local notifyMsgBox=object(NotifyMsgBox)--建立新物件，[superConstructorCall] 繼承類別的建構式(new()方法)呼叫
	--## instance private property
	local _notifyMsgID=id
	local _notifyMessage=message
	local _notifyMsgURL=url
	
	--## instance public property
		
	--## instance private method
	local function onComplete( event )
		if event.action=="clicked" then
			local i = event.index
			if 1 == i then
				-- Do nothing; dialog will simply dismiss
				-- analytics.logEvent("SYSMSG_NO")
			elseif 2 == i then
				-- Open URL if "Learn More" (the 2nd button) was clicked
				-- analytics.logEvent("SYSMSG_YES"..t[1])
				local function responseListener(event)
					if ( event.isError ) then
						print( "WARNING: NotifyMsgBox sending notify-message-clicked Request faild. Network Error.")
					else
						print ( "(INFORM): NotifyMsgBox sending notify-message-clicked Request success. RESPONSE: " .. event.response )
					end
				end
				QllCGIRequest:notifyMsgClickedRequest(AppInfo.q,_notifyMsgID,responseListener)
				system.openURL( _notifyMsgURL )
			end
		end
	end
	--初始化
	local function init()
		if(not AppInfo.infoInitialized) then
			print("ERROR: Initializing object 'NotifyMsgBox' faild. Class 'AppInfo' should be initialized first.")
		end
	end
	--## instance public method
	--顯示彈出視窗
	--*當 msgID = 0 的時候不會彈出
	function notifyMsgBox:show()
		if (_notifyMsgID ~= 0) then
			if (AppInfo.language == AppInfo.LANG_ZH_TW) then
				local alert = native.showAlert( "QLL Company", _notifyMessage, 
					{ "不用，謝謝", "告訴我去哪找" }, onComplete )
			elseif (AppInfo.language == AppInfo.LANG_ZH_CN) then
				local alert = native.showAlert( "QLL Company", _notifyMessage, 
					{ "不用，谢谢", "告诉我去哪找" }, onComplete )
			else
				local alert = native.showAlert( "QLL Company", _notifyMessage, 
					{ "No, Thanks.", "Tell me more..." }, onComplete )            
			end
		end
	end
	--唯讀屬性 notifyMsgID
	function get:msgID()
		return _notifyMsgID
	end
	--唯讀屬性 notifyMessage
	function get:message()
		return _notifyMessage
	end
	--唯讀屬性 notifyMsgURL
	function get:msgURL()
		return _notifyMsgURL
	end
	
	--初始化 並 傳回物件
	return endObject()
end}

--傳回類別
return endClass()

