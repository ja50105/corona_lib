--## package path
packagePath("lib.qll.appInfo.SettingFile")
--## package requirements
local OOP=require("lib.OOP")
local JsonMngr=require("lib.tools.JsonMngr")
--## class: SettingFile
--此類別負責管理App設定檔，並簡化其存取操作。一般由 AppInfo管理，不需要自行建立或操作。
--此類別會在首次被 require()載入時，自動讀取本機設定檔的最新數值。透過 getValue()和 setValue()等方法，就可存取設定檔各項屬性的數值。透過 dataLoaded屬性，則可以確認本機設定檔是否被正確載入。
--在APP被關閉時，此類別會自動將最新的數值寫入設定檔(setting.jsn)當中。
local SettingFile=abstract(class("SettingFile"))
--## static private property
local fileData={}
local _dataLoaded=false
local fileName="setting.jsn"

--## static public property
SettingFile.saveOnChanged=true--若此屬性為true，則會在每次資料修改後進行儲存。
--## static private method
--回應APP關閉事件，在APP關閉時自動存檔
local function systemEvHandeler(ev)
	if(ev.type=="applicationExit") then
		SettingFile:saveFileData()
	end
end
--初始化，類別被首次使用require()載入時執行
local function init()
	SettingFile:loadFileData()
	Runtime:addEventListener( "system", systemEvHandeler)
end
--自動儲存
local function autoSave()
	if(SettingFile.saveOnChanged) then
		SettingFile:saveFileData()
	end
end
--## static public method
--載入資料
function SettingFile:loadFileData()
	local loadedData=JsonMngr:loadTable(fileName)
	if(loadedData) then
		fileData=loadedData
		_dataLoaded=true
	else
		_dataLoaded=false
	end
end
--儲存資料
function SettingFile:saveFileData()
	if(fileData) then
		JsonMngr:saveTable(fileData,fileName)
	else
		print("Error: Saving file 'setting.jsn' faild! File Data cannot be nil.")
	end
end
--設定屬性
--propertyName (string) 要存取的屬性名稱
--value (*) 指定給屬性的新值，不可為nil。
func.setValue{String,Nil,
function(propertyName,value)
	if(value~=nil) then
		fileData[propertyName]=value
		autoSave()
	end
end}
--取得屬性
--propertyName (string) 要存取的屬性名稱
func.getValue{String,
function (propertyName)
	return fileData[propertyName]
end}

--取得 dataLoaded 屬性
--此屬性會指出，本機的設定檔是否被正確載入。若已正確載入，其值為true，否則為false。
function get:dataLoaded()
	return _dataLoaded
end

--初始化
init()
--傳回類別
return endClass()

