local OOP=require("lib.OOP")
--## package path
packagePath("lib.qll.appInfo.AppInfo")

--## package requirements
local SettingFile=require("lib.qll.appInfo.SettingFile")
local QllCGIRequest=require("lib.qll.net.QllCGIRequest")
local json=require("json")
local StringTool=require("lib.tools.StringTool")
local NotifyMsgBox=require("lib.qll.appInfo.NotifyMsgBox")
local RateMsgBox=require("lib.qll.appInfo.RateMsgBox")
local crypto=require("crypto")
local QCoin=require("lib.qll.appInfo.QCoin")

--## class: AppInfo
--QLL的APP一定會有的標準配備。會告訴你 APP的語言、被開過幾次、uid、DeviceID
--AppInfo會連線到伺服器跟它要最新的資訊，updated 屬性會告訴你有沒有成功更新。
--*使用之前一定要先呼叫 initInfo()，不然有些功能無法正常使用！例如 appID,language,market 等屬性，只有在呼叫後才會被填入。
local AppInfo=abstract(class("AppInfo"))--建立新類別

--## static private property
local _appID
local _language
local _market
local _deviceID
local _infoInitialized=false
local _notifyMsgBox
local _rateMsgBox
local numLog=0
local _updated=false
local updateSuccessFuncList={}
local _ratingURL
local _moreAppsList
local _recommendApp

--## static public property
const.RATED_NO="no"
const.RATED_YES="yes"
const.RATED_IGNORED="ignored"

const.MARKET_GOOGLE_PLAY= "googlePlay"
const.MARKET_AMAZON= "amazon"
const.MARKET_APPSTORE= "appStore"
const.MARKET_HAMI= "hami"
const.MARKET_NABI= "nabi"
const.MARKET_BESTA= "besta"
const.MARKET_SAMSUNG= "samsung"

const.LANG_ZH_TW="zh-Hant"
const.LANG_ZH_CN="zh-Hans"
const.LANG_EN="en"
const.LANG_JA="ja"
const.LANG_ES="es"

const.OS_ANDROID="Android"
const.OS_IPHONE="iPhone OS"
const.OS_SIMULATOR="simulator"

--## static private method
--重設設定檔
local function resetSettingFile()
	SettingFile:setValue("q",-1)
	SettingFile:setValue("uid",-1)
	SettingFile:setValue("launchTimes",0)
	SettingFile:setValue("registered",false)
	SettingFile:setValue("rated",AppInfo.RATED_NO)
end
--取得訊息彈出視窗物件
local function getNotifyMsgBox(notifyData)
	if(notifyData and notifyData~="") then
		local notifyDataTable=StringTool:split(notifyData,",")
		id=tonumber(string.sub(notifyDataTable[1],3,#notifyDataTable[1]-2))
		message=string.sub(notifyDataTable[2],3,#notifyDataTable[2]-2)
		url=string.sub(notifyDataTable[3],3,#notifyDataTable[3]-2)
		_notifyMsgBox=NotifyMsgBox:new(AppInfo,id,message,url)
	else
		print("Creating NotifyMsgBox failed, got invalid notifyData form server.")
	end
end
--從伺服器取得數值
--completeFunc (Function) 從伺服器更新後呼叫
local function updateFromServer(completeFunc)
	local function requestHandler(ev)
		local success=false
		--處理傳回的資料
		if(ev.isError) then
			print( "WARNING: Update from server failed. Network Error, Cannot reach server.")
		elseif(ev.response=="") then
			print( "WARNING: Update from server failed. Got empty response from server.")
		else
			local jsonData=json.decode(ev.response)
			if(jsonData.error~="") then
				print( "WARNING: Update from server failed. Server error message:"..jsonData.error)
			else
				--更新數值
				AppInfo.q=jsonData.q
				AppInfo.uid=jsonData.uid
				AppInfo.registered=jsonData.is_register=="Y"
				_ratingURL=jsonData.ratingURL
				_moreAppsList=jsonData.moreAppsList
				_recommendApp=jsonData.recommendApp
				_rateMsgBox=RateMsgBox:new(AppInfo)
				if (jsonData.notify and jsonData.notify~="") then
					 getNotifyMsgBox(jsonData.notify)
				end
				local notifyData=jsonData.notifyData--or {id=2,message="Cool",url="http://google.com"}
				if(notifyData) then
					_notifyMsgBox=NotifyMsgBox:new(AppInfo,notifyData.id,notifyData.message,notifyData.url)
				end
				--更新標籤
				success=true
        -- moogoo, 2013-11-07
        _data = jsonData.data
			end
		end
		_updated=success--設定更新是否成功完成
		--呼叫佇列中的更新成功監聽器
		-- if(success) then
			-- for index, updateSuccessFunc in pairs(updateSuccessFuncList) do
				-- updateSuccessFunc()
			-- end
			-- updateSuccessFuncList={}
		-- end
		--呼叫更新完成監聽器
		if(completeFunc) then
			completeFunc(success)
		end
	end
	if(AppInfo.q>=0) then
		QllCGIRequest:qDataRequest(AppInfo.q,system.getInfo("model"),getLanguage(),requestHandler)
	else
		QllCGIRequest:newQIDRequest(_deviceID,_appID,system.getInfo("model"),getLanguage(),requestHandler)
	end
end
--取得語言
local function getLanguage()
	local lang = system.getPreference( "ui", "language" )						--	Apple
	local language_locale = system.getPreference( "locale", "identifier" )		--  Android
	local language_code = system.getPreference( "locale", "language" )			--  Android
	if system.getInfo("platformName") == "Android" then
		if language_code=="zh" then
			if language_locale=="zh_TW" then
				lang="zh-Hant"
			else
				lang="zh-Hans"
			end
		else
			lang=language_code
		end
	elseif system.getInfo("environment") == "simulator" then
		lang = "zh-Hant"
	end	
	return lang
end
--基本初始化
local function init()
	print("AppInfo DataLoaded:",SettingFile.dataLoaded)
	if(not SettingFile.dataLoaded) then
		resetSettingFile()
	end
	_language=getLanguage()
	_deviceID=system.getInfo( "deviceID" )
	--增加開啟次數
	AppInfo.launchTimes=AppInfo.launchTimes+1
	QCoin:init(AppInfo)
end
--檢驗是否已被手動初始化
local function testInfoInit(propName)
	if(not _infoInitialized) then
		print("ERROR: Getting property '"..propName.."' form 'AppInfo' failed. Function 'initInfo()' should be called first.")
	end
end
--## static public method

--q 是裝置、使用者、App三者組合的識別碼。如果 AppInfo 初始化失敗，q會傳回-1
--此屬性會在呼叫 initInfo()時從伺服器更新。即使寫入此屬性，伺服器端的值也不會被更新。
function set:q(arg)
	if(arg~=nil) then
		if(arg>=0) then
            -- setting.jsn有q以後就鎖住不給更新, 2013-01-21, moogoo
            -- << SettingFile:setValue("q",arg)
            ---------------------
            if AppInfo.q <= 0 then
                -- 第一次AppInfo.q = -1
                SettingFile:setValue("q",arg)
            elseif AppInfo.q ~= arg then
                -- AppInfo.q 和 AppInfo.init()後回傳的q不同 (server bug!)
                -- 送log到server去分析
                local function networkListener( event )
                    if ( event.isError ) then
                        print( "Network error! (set:q)")
                    else
                        --print ( "RESPONSE: " .. event.response )
                    end
                end
                local u = string.format("https://web.qll.co/api/log/%d/warn?diffq=%d", AppInfo.q, arg)
                network.request( u, "GET", networkListener )
            end
            -->>
		else
			print("ERROR: Setting AppInfo.q failed. Invalid argument Input.")
		end
	end
end
function get:q()
	return SettingFile:getValue("q")
end
--使用者ID
--此屬性會在呼叫 initInfo()時從伺服器更新。即使寫入此屬性，伺服器端的值也不會被更新。
function set:uid(arg)
	if(arg~=nil) then
		if(arg>=0) then
			SettingFile:setValue("uid",arg)
		else
			print("ERROR: Setting AppInfo.uid failed. Invalid argument Input.")
		end
	end
end
function get:uid()
	return SettingFile:getValue("uid")
end
--app的開啟次數
--數值會在每次 App啟動時自動累加，因此通常不必手動修改此屬性。
function set:launchTimes(arg)
	if(arg~=nil) then
		if(arg>=0 and type(arg)=="number") then
			SettingFile:setValue("launchTimes",arg)
		else
			print("ERROR: Setting AppInfo.launchTimes failed. Invalid argument Input.")
		end
	end
end
function get:launchTimes()
	return SettingFile:getValue("launchTimes")
end
--此屬性會指出使用者是否已註冊
--此屬性會在呼叫 initInfo()時從伺服器更新。即使寫入此屬性，伺服器端的值也不會被更新。
function set:registered(arg)
	if(arg~=nil) then
		if(type(arg)=="boolean") then
			SettingFile:setValue("registered",arg)
		else
			print("ERROR: Setting AppInfo.registered failed. Invalid argument Input.")
		end
	end
end
function get:registered()
	return SettingFile:getValue("registered")
end
--此屬性會指出使用者是否已為這個App評分
--*如要對此屬性進行比較運算，可使用 AppInfo所提供的常數值。
function set:rated(arg)
	if(arg~=nil) then
		if(type(arg)=="string") then
			SettingFile:setValue("rated",arg)
		else
			print("ERROR: Setting AppInfo.rated failed. Invalid argument Input.")
		end
	end
end
function get:rated()
	return SettingFile:getValue("rated")
end
--這個URL會指向這個 App在商店的評分頁面(例如 GooglePlay或 AppStore)
function get:ratingURL()
	testInfoInit("ratingURL")
	return _ratingURL
end
--裝置的ID
function get:deviceID()
	return _deviceID
end
--裝置的系統平台
--*如要對此屬性進行比較運算，可使用 AppInfo所提供的常數值。
function get:os()
	return system.getInfo("platformName")
end
--每個app獨特且不重複的ID。即使是同一個app，在發佈到不同的商店時，仍會使用不同的ID。其值即呼叫 initInfo()時指定的參數 appID。
function get:appID()
	testInfoInit("appID")
	return _appID
end
--這個 App所使用的語言
--*如要對此屬性進行比較運算，可使用 AppInfo所提供的常數值。
function get:language()
	testInfoInit("language")
	return _language
end
--這個 App所被發佈的商店平台(例如 App Store或 Google Play)
--*如要對此屬性進行比較運算，可使用 AppInfo所提供的常數值。
function get:market()
	testInfoInit("market")
	return _market
end
--NotifyMsgBox 物件，如果並未成功從伺服器取得 notifyMsgBox要顯示的訊息，則這個屬性將會是 nil。
function get:notifyMsgBox()
	testInfoInit("notifyMsgBox")
	return _notifyMsgBox
end
--此屬性是一個 RateMsgBox 物件。
--rateMsgBox 是給用戶的給好評彈出訊息視窗。在 AppInfo成功從伺服器更新資料前，這個屬性會是 nil。
function get:rateMsgBox()
	testInfoInit("rateMsgBox")
	return _rateMsgBox
end
--取得其他App的資料清單
--會傳回一個清單，其中的每個項目都是一個 Table，包含兩個屬性：appURL和 iconURL。
function get:moreAppsList()
	testInfoInit("moreAppsList")
	return _moreAppsList
end
--取得建議的App資料
--會傳回一個Table，包含兩個屬性：appURL和 iconURL。
function get:recommendApp()
	testInfoInit("recommendApp")
	return _recommendApp
end
--applicationKey 等屬性是否已被手動初始化。如果沒有，嘗試存取未初始化的屬性會傳回錯誤。
function get:infoInitialized()
	return _infoInitialized
end

-- 每個App的特殊data, 2013-11-07, moogoo
function get:data()
	--testInfoInit("foo")
	return _data
end

--AppInfo 是否已成功從伺服器更新資料。
function get:updated()
	return _updated
end
--裝置的螢幕像素寬度
function get:devicePixelWidth()
	return (display.contentWidth-display.screenOriginX*2)/display.contentScaleX
end
--裝置的螢幕像素高度
function get:devicePixelHeight()
	return (display.contentHeight-display.screenOriginY*2)/display.contentScaleY
end
--QCoin類別，提供多個屬性與方法以便存取錢幣金額
function get:QCoin()
	return QCoin
end


--設定必要的資訊並初始化
--此處所要求的參數，是只能由APP開發者手動輸入設定的屬性。一旦參數被輸入，便會填入 AppInfo 類別的屬性當中供存取。
--呼叫此方法後，infoInitialized 屬性便會被設為 true。同時 AppInfo會自動嘗試從伺服器更新資訊，如果更新成功 updated屬性會被設為 true。
--appID  App 的識別碼。必須為數字代號。
--market 賣場，可使用常數： AppInfo.MARKET_GOOGLE_PLAY ,AppInfo.MARKET_AMAZON ,AppInfo.MARKET_APPSTORE 等...
--[updateCompleteFunc] (Function) 當此物件從伺服器更新資料完成，會呼叫這個函數，並傳入參數 success (Boolean)，若成功更新會傳回true。
func.initInfo{Number,String,Function,2,
function (appID,market,updateCompleteFunc)
	_infoInitialized=true
	if(type(appID)==String or type(appID)==Number) then
		_appID=appID
		_market=market
		updateFromServer(updateCompleteFunc)--有 appID 就可以跟伺服器要資料了。
	else
		print("ERROR: Calling AppInfo:initInfo() failed. Argument 'appID' should be a String or Number.")
	end
end}

--重新嘗試從伺服器更新資料
--呼叫initInfo()方法便會自動嘗試從伺服器更新資料，若資料更新失敗，可用此函數重新嘗試更新。
--未呼叫過 initInfo() 就呼叫此方法會擲回錯誤。
--[updateCompleteFunc] (Function) 當此物件從伺服器更新資料完成，會呼叫這個函數，並傳入參數 success (Boolean)，若成功更新會傳回true。
func.reupdateFromServer{Nil,
function (updateCompleteFunc)
	if(_infoInitialized) then
		updateFromServer(updateCompleteFunc)
	else
		print("ERROR: Calling function 'reupdateFromServer()' failed. Function 'initInfo()' should be called first.")
	end
end}

--從伺服器更新成功時，呼叫指定的函數
--更新失敗時並不會呼叫函數。這個方法可以被呼叫多次，以指定多個不同的 updateSuccessFunc。
--updateSuccessFunc (Function) 在成功更新時要呼叫的函數。沒有任何參數會被傳入。
-- func.callOnUpdated{Function,
-- function (updateSuccessFunc)
	-- if(_updated) then
		-- updateSuccessFunc()
	-- else
		-- table.insert(updateSuccessFuncList,updateSuccessFunc)
	-- end
-- end}
--將所有透過 callOnUpdated 設置的函數清除
-- func.removeAllUpdatedCall{
-- function ()
	-- updateSuccessFuncList={}
-- end}


--確認是否有網路連線
--listenerFunc (function) 在確認網路後觸發的函數。觸發時會傳入參數 availavle (boolean)，表示是否有網路可用。
--格式：function listenerFunc(availavle) end
func.getNetAvailable{Function,
function (listenerFunc)
	local function networkListener( event )
		local availavle=not event.isError and event.status==200
		listenerFunc(availavle)
	end
-- Access Google
	network.request( "http://www.google.com", "GET", networkListener)
end}

--取得新的 keyHash
--keyHash是由 hashSeed(從伺服器取得) 和 uid 以及一個自訂的固定字串，經過演算法得到。由於 keyHash會隨著 hashSeed會改變，因此可以確保帶有 keyHash的 CGI url無法被重複呼叫，來作為一種防弊措施。
--listenerFunc (Function) 在取得 keyHash後會被觸發，觸發時會傳入參數 keyHash (String)，如果產生 keyHash時發生錯誤，則會傳入 nil。
func.getNewKeyHash{Function,
function (listenerFunc)
	local function hashSeedRequestHandler(ev)
		local keyHash
		if(ev.isError) then
			print("ERROR: Getting hashSeed from server failed. Network Error.")
		else
			local jsonData=json.decode(ev.response)
			local hashSeed=tonumber(jsonData.hash_seed)
			keyHash=crypto.hmac( crypto.sha256, tostring((tonumber(hashSeed) + tonumber(AppInfo.uid))), "qllkeymonster888168")
		end
		listenerFunc(keyHash)
	end
	QllCGIRequest:getHashSeedRequest(AppInfo.q,hashSeedRequestHandler)
end}

--語言測試
--language (String) 要測試的語系名稱，可使用常數如 AppInfo.LANG_ZH_TW 來指定其值。忽略參數或傳入nil，可以結束測試，並使用偵測到的語言。
func.languageTest{String,0,
function (language)
	if(language) then
		_language=language
	else
		_language=getLanguage()
	end
end}

--初始化
init()
--傳回類別
return endClass()

