require("lib.OOP")
--## package path
packagePath("lib.display.DisplayObjectContainer")
--## package requirements
local InteractiveObject=require("lib.display.InteractiveObject")
local DisplayObject=require("lib.display.DisplayObject")
local Event=require("lib.events.Event")

--## class:DisplayObjectContainer >----
-- 所有用來作為 DisplayObject 容器的物件都繼承自 DisplayObjectContainer類別。
-- 所有繼承自這個類別的物件都可藉由呼叫 insert()方法來把物件加入容器。
local DisplayObjectContainer=abstract(virtualClass("DisplayObjectContainer",display.newGroup,InteractiveObject))--建立新類別 DisplayObjectContainer
--## static private property

--## static public property

--## static private method

--## static public method
--建立實例(物件)
func.new{
function ()
	local displayObjectContainer=virtualObject(DisplayObjectContainer,display.newGroup())--建立新物件 displayObjectContainer
	--## instance private property
	local childList={}
	local childDict={}
	--## instance public property
	
	
	--## instance private method
	--回應物件被加入容器或刪除(若是物件被加到其他容器，會把物件從清單剔除)
	local function onObjAdded(ev)
		local child=ev.target
		childDict[child] = nil;
		table.remove(childList,table.indexOf(childList,child))
		child:removeEventListener(Event.ADDED, onObjAdded)
		child:removeEventListener(Event.REMOVED, onObjAdded)
	end
	--發出事件並設置子物件的監聽器
	local function setChildEvent(child)
		local event=Event:new(Event.ADDED)
		child:dispatchEvent(event)
		child:addEventListener(Event.ADDED, onObjAdded)
		child:addEventListener(Event.REMOVED, onObjAdded)
	end
	--物件初始化
	local function init()
		
	end
	--## instance public method
	--將物件加入容器
	--return 傳回物件所在的索引位置
	func.insert{DisplayObject,
	function (child)
		if(not displayObjectContainer.factoryObject.insert) then
			err(displayObjectContainer,nil,"Cannot insert object to this object. This Object had already been removed.")
		end
		if(not (childDict[child] or child.isRemoved))then
			displayObjectContainer.factoryObject:insert(child)
			childDict[child] = true;
			table.insert(childList,child)
			child:internal().parent=displayObjectContainer;
			setChildEvent(child)
		else
			warn(displayObjectContainer,nil,"Attempt to insert an object, which is removed or already a child of this container.")
		end
		return #childList;
	end
	,Number}
	--加入物件到指定索引位置
	--index:物件被加入後在childList中的索引位置。childList可透過 getChildList()方法取得。
	--child:要加入容器的物件
	func.insertAt{Number,DisplayObject,2,
	function (index,child)
		if(not displayObjectContainer.factoryObject.insert) then
			err(displayObjectContainer,nil,"Cannot insert object to this object. This Object had already been removed.")
		end
		if(not (childDict[child] or child.isRemoved))then
			local maxIndex=#childList+1
			if(index>0 and index<=maxIndex)then
				displayObjectContainer.factoryObject:insert(index,child)
				childDict[child] = true;
				table.insert(childList,index,child)
				child:internal().parent=displayObjectContainer;
				setChildEvent(child)
			else
				err(displayObjectContainer,nil,"Attempt to insert an object with invalid index.")
			end
		else
			warn(displayObjectContainer,nil,"Attempt to insert an object, which is removed or already a child of this container.")
		end
	end
	}
	--從容器中移除指定的物件。
	--child:要從容器中移除的物件。
	--注意：此方法與corona SDK GroupObject的 remove方法不同，不允許輸入數字索引當作參數。
	func.remove{DisplayObject,
	function (child)
		if(childDict[child])then
			child:removeSelf();
		else
			err(displayObjectContainer,nil,"Attempt to remove an object,which is not a child of this container.")
		end
	end
	}
	--從容器中移除位於指定索引處的物件。
	--index:要從容器中移除之物件的數字索引。
	func.removeAt{Number,
	function (index)
		local maxIndex=#childList
		if(index>0 and index<=maxIndex)then
			local child = childList[index];
			child:removeSelf();
		else
			err(displayObjectContainer,nil,"Attempt to remove an object with invalid index.")
		end
	end
	}

	--容器中容納的物件總數
	function get:numChildren()
		return #childList
	end
	--取得容器中位於指定索引的物件，索引的最大值可由 numChildren屬性獲得
	func.getChildAt{Number,
	function (index)
		return childList[index]
	end
	,AlignableObject}
	--取得當前子物件清單的副本
	func.getChildList{
	function ()
		return table.copy(childList)
	end
	,Table}
	--移除物件的顯示
	--物件被移除時 isRemoved 屬性會被設為true，並且會發出 REMOVED 事件
	--容器的子物件也會被移除，並且皆會發出 REMOVED事件。
	local superRemoveSelf=displayObjectContainer.removeSelf
	function override:removeSelf()
		if(not displayObjectContainer.isRemoved) then
			-- print("CON>>",displayObjectContainer)
			--手動移除所有子物件，以保證 REMOVED事件被觸發
			local childListCopy=table.copy(childList)
			for i, child in pairs(childListCopy) do
				child:removeSelf()
			end
			superRemoveSelf()
			-- print("<<CON",displayObjectContainer)
		end
	end
	--初始化 並 傳回物件
	init()
	return endObject()
end}

--傳回類別
return endClass()

