require("lib.OOP")
--## package path
packagePath("lib.display.AlignableObject")
--## package requirements
local DisplayObject=require("lib.display.DisplayObject")
local Event=require("lib.events.Event")
--## class:AlignableObject >----
-- AlignableObject 具有被排列時所需要的各種屬性。因此繼承自 AlignableObject 類別的物件，都將可以被排列。
--如果 AlignableObject設置了固定的寬與高(aWidth, aHeight)，那麼不論其實際大小為何，被放到Div中排列時，都會以設置的寬高為準。
--但是如果將寬(或高)的值設為nil，則寬(或高)會等於 contentWidth(或 contentHeight)，並隨著物件實際大小而改變。(但Div物件則有些不同，請自行參閱說明)
--## events
--Event.ALIGN_CHANGED:當margin_left,margin_right...,aWidth,aHeight 等與對齊相關的屬性改變後觸發

local AlignableObject=abstract(class("AlignableObject",DisplayObject))--建立新類別 AlignableObject
--## static private property
--## static public property
--## static private method
--## static public method
--建立實例(物件)
func.new{
function ()
	local alignableObject=object(AlignableObject)--建立新物件 alignableObject
	--## instance private property
	local _margin_top=0
	local _margin_bottom=0
	local _margin_left=0
	local _margin_right=0
	local _aWidth
	local _aHeight
	--## instance public property
	--## instance private method
	--屬性被改變
	local function onChanged()
		alignableObject:dispatchEvent(Event:new(Event.ALIGN_CHANGED))
	end
	--## instance public method
	--margin_top
	function get:margin_top()
		return _margin_top
	end
	function set:margin_top(arg)
		_margin_top=arg
		onChanged()
	end
	--margin_bottom
	function get:margin_bottom()
		return _margin_bottom
	end
	function set:margin_bottom(arg)
		_margin_bottom=arg
		onChanged()
	end
	--margin_left
	function get:margin_left()
		return _margin_left
	end
	function set:margin_left(arg)
		_margin_left=arg
		onChanged()
	end
	--margin_right
	function get:margin_right()
		return _margin_right
	end
	function set:margin_right(arg)
		_margin_right=arg
		onChanged()
	end
	--排列時的寬，預設為自動，其值會與 width 相等。將值設為 nil可以變成自動。
	function get:aWidth()
		return _aWidth or alignableObject.width
	end
	function set:aWidth(arg)
		_aWidth=arg
		onChanged()
	end
	--排列時的高，預設為自動，其值會與 height 相等。將值設為 nil可以變成自動。
	function get:aHeight()
		return _aHeight or alignableObject.height
	end
	function set:aHeight(arg)
		_aHeight=arg
		onChanged()
	end
	--設定四邊的Margin
	--如果只輸入第一個參數，會將四邊的Margin設為相同的值
	--其他情況下，被省略的參數會被設為0
	func.setMargin{Number,Number,Number,Number,0,
	function(left,right,top,bottom)
		if(left and not (right or top or bottom)) then
			_margin_top=left
			_margin_bottom=left
			_margin_left=left
			_margin_right=left
		else
			_margin_top=top or 0
			_margin_bottom=bottom or 0
			_margin_left=left or 0
			_margin_right=right or 0
		end
		onChanged()
		return alignableObject;
	end
	,AlignableObject}
	--設定寬高尺寸
	func.setAlignSize{Number,Number,
	function(aWidth,aHeight)
		_aWidth=aWidth
		_aHeight=aHeight
		onChanged()
		return alignableObject;
	end
	,AlignableObject}

	return endObject()
end}

--傳回類別
return endClass()

