require("lib.OOP")
--## package path
packagePath("lib.display.Image")
--## package requirements
local InteractiveObject=require("lib.display.InteractiveObject")
-- local DisplayNewImage=requireVirtual("DisplayNewImage",display.newImage,InteractiveObject)--虛擬類別
--## class:Image
--基本的靜態圖片物件。透過將外部圖檔載入來產生。
local Image=virtualClass("Image",display.newImage,InteractiveObject)--建立新類別 Image
--## static private property
--## static public property
--## static private method
--## static public method
--建立 Image物件
--imagePath 圖片路徑
--baseDirectory 根目錄資料夾。省略或輸入 nil會以 system.ResourceDirectory作為預設值
--x,y 物件的初始位置
--isFullResolution 載入大圖時，是否使用完整大小(不自動縮放)，預設值為 true (與官方API的預設值相反)
func.new{String,UserData,Number,Number,Boolean,1,
function (imagePath, baseDirectory, x,y,isFullResolution)
	local factoryObj=display.newImage(imagePath,baseDirectory or system.ResourceDirectory,x or 0,y or 0,isFullResolution or (isFullResolution==nil))
	if(factoryObj==nil)then err(nil,nil,"Failed to find Image '"..imagePath.."'")end;
	local image=virtualObject(Image,factoryObj)--建立新物件 image，[superConstructorCall] 繼承類別的建構式(new()方法)呼叫
	--## instance private property
	--## instance public property
	--## instance private method
	--物件初始化
	local function init()
	end
	--## instance public method
	--初始化 並 傳回物件
	init()
	return endObject()
end}

--傳回類別
return endClass()

