require("lib.OOP")
--## package path
packagePath("lib.display.Stage")
--## package requirements
local DisplayObjectContainer=require("lib.display.DisplayObjectContainer")
local Event=require("lib.events.Event")
local InteractiveObject=require("lib.display.InteractiveObject")
--## class:Stage >----
--Stage 類別只有一個實例，這個實例是整個 Corona 執行階段中，最上層的顯示物件容器。所有被建立的顯示物件都在這個容器中。
--## events
--Event.FOCUS_ASSIGNED:呼叫 assignFocus()方法，取得交點後觸發。
--Event.FOCUS_RESIGNED:呼叫 resignFocus()方法，歸還焦點後觸發。
local Stage=abstract(virtualClass("Stage",display.getCurrentStage,DisplayObjectContainer))--建立新類別 Stage，[extendsClass] 繼承的類別
--## static private property
local stage
local _focus
local oldGetCurrentStage=display.getCurrentStage
local _showFocusObject=false;
local focusRect;
--## static public property
	
--## static private method

--初始化
local function init()
	stage=virtualObject(Stage,display.getCurrentStage())--建立新物件 stage，[superConstructorCall] 繼承類別的建構式(new()方法)呼叫
	--## instance private property
	--## instance private method
	--新的getCurrentStage 含警告文字
	local function getCurrentStage()
		if(not warned) then
			print("WARNING: Attempt to call a discarded function 'display.getCurrentStage()'. The 'stage' you are getting is different from the 'stage' property on DisplayObject.(Recommended to use 'stage' property instead.)")
			warned=true
		end
		return oldGetCurrentStage()
	end
	--取代舊的display.getCurrentStage
	local function replaceGetStage()
		display.getCurrentStage=getCurrentStage
	end
	--於物件被移除時自動歸還焦點
	local function onObjRemoved(event)
		local success = stage:resignFocus(event.target);
	end	
	local function drawFocusRect()
		if(focusRect)then
			focusRect:removeSelf()
			focusRect=nil;
		end
		if(_showFocusObject and _focus)then
			local x, y=_focus:localToContent(0,0);
			focusRect=display.newRect( x, y, _focus.width, _focus.height );
			focusRect:setFillColor( 0,0,0,0 )
			focusRect.strokeWidth = 4
			focusRect:setStrokeColor( 1, 1, 0 );
			focusRect.anchorX=_focus.anchorX;
			focusRect.anchorY=_focus.anchorY;
		end
	end
	--## instance public method
	--將物件指定為焦點
	--物件本身的 focusPriority屬性為取得焦點時的優先級，只有在物件的優先級大於其他物件，或焦點未被占用的狀況下，才會成功取得。
	--object 要取得焦點的物件
	--force 忽略優先級強制取得焦點。此做法有風險，請謹慎使用。預設為 false。
	--如果成功取得焦點，會傳回true，否則為false
	func.assignFocus{InteractiveObject,Boolean,1,
	function (object,force)
		local focusPriority=object.focusPriority;
		local currentPriority=0
		if(_focus)then
			currentPriority=_focus.focusPriority;
		end
		if(not _focus or force or currentPriority<0 or currentPriority<focusPriority) then
			_focus = object;
			currentPriority=focusPriority;
			stage.factoryObject:setFocus(_focus);
			stage:dispatchEvent(Event:new(Event.FOCUS_ASSIGNED));
			object:addEventListener(Event.REMOVED ,onObjRemoved)
			drawFocusRect()
		end
		return _focus==object
	end
	,Boolean}
	--使物件將焦點歸還
	--object 要歸還焦點的物件
	--只有確認該物件占用焦點時，焦點才會被歸還(重置)。以防其他物件占用焦點時被意外重置。
	--傳回值 當焦點成功歸還(回到沒有焦點的狀態時)，才會傳回 true。
	func.resignFocus{InteractiveObject,
	function (object)
		local valid=(_focus==object);
		if(valid) then
			_focus=nil
			stage.factoryObject:setFocus(nil)
			stage:dispatchEvent(Event:new(Event.FOCUS_RESIGNED))			
			object:removeEventListener(Event.REMOVED,onObjRemoved)
			drawFocusRect()
		end
		return valid
	end
	,Boolean}
	--目前占用焦點的物件，為 nil或一個 InteractiveObject。
	function get:focus()
		return _focus
	end
	--是否在畫面上將取得焦點的物件，用黃色方框標示出來。
	function set:showFocusObject(value)
		_showFocusObject = value;
		drawFocusRect();
	end
	function get:showFocusObject()
		return _showFocusObject;
	end

	replaceGetStage()
	endObject()
end

--## static public method
--唯讀屬性 stage
function get:stage()
	return stage--總是傳回同一個stage實例
end

init()
--傳回類別
return endClass()

