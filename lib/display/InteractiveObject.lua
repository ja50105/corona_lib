require("lib.OOP")
--## package path
packagePath("lib.display.InteractiveObject")
--## package requirements
local Event=require("lib.events.Event")
local TouchEvent=require("lib.events.TouchEvent")
local AlignableObject=require("lib.display.AlignableObject")

--## class:InteractiveObject
-- InteractiveObject 可以處理與使用者的互動，並發出互動事件，例如 Touch , Tap 等(這是概念意義上的功能，此功能未被實作)。
-- 此類別並未實作接收與回應使用者互動的核心功能，因此必須被「官方API當中，可與使用者互動(能發出 touch與 tap事件)之顯示物件製成的虛擬類別」繼承，方能正常運作。以任何其他方式繼承此類別，都會導致錯誤。
-- 此類別整合 CoronaAPI與 OOP Lib提供的兩套事件機制，使其運作不衝突。
-- focusPriority,BlockTouch是 InteractiveObject物件特有的屬性。他們協助簡化多個重疊物件對 Touch(Tap)事件回應，以及從 Stage取得 focus的優先權調度(詳見屬性的說明)。
local InteractiveObject=abstract(class("InteractiveObject",AlignableObject))--建立新類別 InteractiveObject
--## static private property
-- local occupyingObj--占用事件之物件<<<<廢棄>>>>
--## static public property
--## static private method
-- local function enterFrameHandler(ev)
	-- occupyingObj=nil
-- end
--[[ <<<<廢棄>>>>
--佔領事件
local function occupyEvent(object)
	local success=false
	if(object) then
		if(not occupyingObj or object==occupyingObj or object.focusPriority>occupyingObj.focusPriority) then--權限足夠則傳回許可並佔有事件
			if(object.focusPriority>0) then--特殊情況，當優先級<=0，則傳回許可但不佔有該事件。
				occupyingObj=object
			end
			success=true
		end
	else
		occupyingObj=nil
	end
	print("OCC",object,success)
	return success
end
]]--
local function classInit()
	-- Runtime:addEventListener("enterFrame", enterFrameHandler)<<<<廢棄>>>>
end
--## static public method
--建立實例(物件)
func.new{
function ()
	local interactiveObject=object(InteractiveObject)--建立新物件 interactiveObject
	--## instance private property
	local super=getSuperProps();--設置Super
	local listenerAdded=false
	local touchProxyDict={}
	local tapProxyDict={}
	--## instance public property
	interactiveObject.focusPriority=1--攔截焦點的優先級，會影響呼叫 stage:assignFocus()的結果。相同優先級的物件，先呼叫者會取得焦點。較大優先級的物件，即使較晚呼叫，仍會取得焦點。
	interactiveObject.blockTouch=false--是否阻擋 touch和 tap事件穿透到下方的物件(及上層容器)。若設為true，則還必須使用 addEventListener()方法監聽 touch(或 tap)事件，才會產生效用。
	--## instance private method
	--## instance public method
	--發出事件
	--event (object Event) 要發出的事件，必須指定table.name屬性
	override.dispatchEvent{Event,
	function (event)
		--使用原生事件機制發出事件
		event.target=event.target or interactiveObject
		if(interactiveObject.factoryObject.dispatchEvent) then
			interactiveObject.factoryObject:dispatchEvent(event)
		end
		--保險起見，自訂事件機制也發出事件(以便回應於父輩附加的EventListener)
		local eventName=event.name
		if(eventName~=TouchEvent.TOUCH and eventName~=TouchEvent.TAP) then
			super:dispatchEvent(event)
		end
	end}
	--加入事件監聽器
	--eventName (string) 監聽的事件名稱
	--listener (function) 作為監聽器的函數
	override.addEventListener{String,Function,
	function (eventName,listener)
		--加入事件轉送監聽器
		if(eventName==TouchEvent.TOUCH or eventName==TouchEvent.TAP) then
			-- 建立事件轉送函數
			local function proxyHandler(evTable)
				local event=Event:toEvent(evTable,TouchEvent:new("",0,0));
				event.target=interactiveObject;--修正指向的目標，從 factoryObject修正為 interactiveObject
				listener(event);
				return interactiveObject.blockTouch
			end	
			if(eventName==TouchEvent.TOUCH and not touchProxyDict[listener]) then
				touchProxyDict[listener]=proxyHandler
				interactiveObject.factoryObject:addEventListener(TouchEvent.TOUCH, proxyHandler)
			elseif(eventName==TouchEvent.TAP and not tapProxyDict[listener]) then
				tapProxyDict[listener]=proxyHandler
				interactiveObject.factoryObject:addEventListener(TouchEvent.TAP, proxyHandler)
			end
			super:addEventListener(eventName, listener)
		elseif(not interactiveObject.isRemoved) then--其他時候由原生事件機制處理
			--小缺陷：此情況下，由 factoryObject發出的原生事件的 target屬性將指向 factoryObject而非 interactiveObject
			interactiveObject.factoryObject:addEventListener(eventName,listener)
		else--如果物件被移除顯示，使得原生事件機制不可用，則以自訂事件機制代管
			super:addEventListener(eventName,listener)
			-- print("Error:Calling addEventListener() on "..tostring(interactiveObject).." failed. This object had been removed.")
		end
	end}
	--移除事件監聽器
	--eventName (string) 移除監聽的事件名稱
	--listener (function) 作為監聽器的函數
	override.removeEventListener{String,Function,
	function (eventName,listener)
		--移除事件轉送監聽器
		if(interactiveObject.factoryObject and (eventName==TouchEvent.TOUCH or eventName==TouchEvent.TAP)) then
			if(eventName==TouchEvent.TOUCH and touchProxyDict[listener]) then
				interactiveObject.factoryObject:removeEventListener(TouchEvent.TOUCH, touchProxyDict[listener])
				touchProxyDict[listener]=nil
			elseif(eventName==TouchEvent.TAP and tapProxyDict[listener]) then
				interactiveObject.factoryObject:removeEventListener(TouchEvent.TAP, tapProxyDict[listener])
				tapProxyDict[listener]=nil
			end
			super:removeEventListener(eventName, listener)
		elseif(not interactiveObject.isRemoved) then--其他時候由原生事件機制處理
			interactiveObject.factoryObject:removeEventListener(eventName,listener)
		else--如果物件被移除顯示，使得原生事件機制不可用，則以自訂事件機制代管。
			super:removeEventListener(eventName,listener)
		end
	end}
	--傳回物件
	return endObject()
end}

classInit()
--傳回類別
return endClass()

