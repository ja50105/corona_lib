require("lib.OOP")
--## package path
packagePath("lib.events.EventDispatcher")
--## package requirements
local Event=require("lib.events.Event")
--## class:EventDispatcher
--甚麼？只有Corona API創造的物件有事件可以用？大不了我自己做一個！那就是這個，用法完全一樣而且更聰明。
--如果你呼叫了兩次一模一樣的addEventListener，這裡只會登記一次(但是Corona很笨，他會註冊兩次然後在觸發的時候呼叫兩次。)
--想用事件嗎？繼承 EventDispatcher吧！
--所有繼承自此類別的物件將可使用事件機制。其使用方式與Corona官方事件API幾乎完全相同。
local EventDispatcher=abstract(class("EventDispatcher"))
--## static public method
--建立實例(物件)
function EventDispatcher:new()
	local eventDispatcher=object(EventDispatcher)
		--## instance private property
	local eventFuncListDict={}
	
	--## instance private method
	
	--## instance public method
	--發出事件
	--event (object Event) 要發出的事件。如果未指定event.target屬性，則會被自動指定為發出事件的物件本身。
	func.dispatchEvent{Event,
	function (event)
		event.target=event.target or eventDispatcher
		local funcList = eventFuncListDict[event.name]
		if(funcList) then
			for i = 1, #funcList do
				if(funcList[i]) then
					funcList[i](event)
				end
			end
		end
	end}
	
	---------------由於官方API當中並沒有此方法，為了保持與後續繼承的一致性，暫時將它關閉
	--檢驗是否有針對指定事件的監聽器存在
	--eventName (String) 監聽的事件名稱
	--傳回值 (Boolean) 若找到監聽器則傳回 true
	-- func.hasEventListener{String,
	-- function (eventName)
		-- local funcList = eventFuncListDict[eventName]
		-- local listenerExist=funcList~=nil and #funcList>0
		-- return listenerExist
	-- end,Boolean}
	
	
	--加入事件監聽器
	--eventName (string) 監聽的事件名稱
	--listener (function) 作為監聽器的函數
	func.addEventListener{String,Function,
	function (eventName,listener)
		if(eventFuncListDict[eventName]==nil) then
			eventFuncListDict[eventName]={}
		end
		local funcList=eventFuncListDict[eventName]
		local index=table.indexOf(funcList, listener)
		if(index==nil) then
			table.insert(funcList,listener)
		end
	end}
	--移除事件監聽器
	--eventName (string) 移除監聽的事件名稱
	--listener (function) 作為監聽器的函數
	func.removeEventListener{String,Function,
	function (eventName,listener)
		local funcList=eventFuncListDict[eventName]
		if(funcList) then
			local index=table.indexOf(funcList, listener)
			if(index~=nil) then
				table.remove(funcList,index)
			end
		end
	end}
	--傳回物件
	return endObject()
end

--傳回類別
return endClass()

