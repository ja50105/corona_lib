require("lib.OOP")
--## package path
packagePath("lib.events.Event")
--## package requirements
--## class: Event
--它是 EventDispatcher的好夥伴，在呼叫 dispatchEvent()方法時，記得塞 Event物件給他讓他幫你快遞。 當事件被觸發時，listener就會收到 Event物件當禮物。
--Event 是最基礎的事件類別，只提供事件的基本資訊：name和 target。如果希望物件能夠攜帶更多的資訊，請繼承這個類別，自行擴充。
--就許多事件而言，例如 Event 類別常數所代表的事件，這些基本資訊就夠用了。然而，其它事件可能需要更詳細的資訊才夠。 例如，與螢幕觸碰相關聯的事件，會需要包含有關按一下事件的發生位置，以及是否在拖曳等額外資訊。 您可以擴充 Event 類別，藉此將這類額外資訊傳遞至事件偵聽程式，TouchEvent 類別就是如此作業。
local Event=class("Event")--建立新類別

--## static public property
const.SAVED="saved"
const.LOADED="loaded"
const.UPDATED="updated"
const.COMPLETE="complete"
const.CHANGED="changed"
const.SELECT="select"
const.ENTER_FRAME="enterFrame"
const.TO_FRONT="toFront"
const.TO_BACK="toBack"
const.ALIGN_CHANGED="alignChanged"
const.ALIGN_COMPLETE="alignComplete"
const.FOCUS_ASSIGNED="focusAssigned"
const.FOCUS_RESIGNED="focusResigned"
const.SCROLLING="scrolling"
const.ADDED="added"
const.REMOVED="removed"
const.UP="up"
const.DOWN="down"
const.STOPPED="stopped"

--## static public method
--將 table轉換成 Event物件
--會把 table上的屬性全部附加到 提供的 Event物件上。若嘗試附加該 Event物件沒有的屬性，會傳回警告訊息。
--evTable (Table) 要轉換的 table
--傳回值 (Event)
func.toEvent{Table,Event,
function (evTable,event)
		for key,value in pairs(evTable) do
			event[key]=value
		end
		return event
end,Event}

--建立實例(物件)
--eventName (String) 事件名稱。可以使用常數：Event.TOUCH,Event.TAP 等
func.new{String,
function (eventName)
	local event=object(Event)--建立新物件
	--## instance private property
	
	--## instance public property
	event.name=""--事件的名稱
	event.target=nil--發出事件的物件
	
	--## instance private method
	--Initialize instance
	local function init()
		event.name=eventName
	end
	
	--## instance public method
	
	--初始化 並 傳回物件
	init()
	return endObject()
end}

--傳回類別
return endClass()