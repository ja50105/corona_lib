--## package path
packagePath("lib.tools.JsonMngr")

--## package requirements
local OOP=require("lib.OOP")
local json = require("json")

--## class: JsonMngr
--把 Table給我，我會幫你變成 json檔案。我也可以反過來。
local JsonMngr=abstract(class("JsonMngr"))

--## static public property

--## static public method
--儲存table
--t 要被儲存的table
--filename 儲存檔案路徑
--directory 檔案目錄 預設值 system.DocumentsDirectory
--傳回值： (boolean) 若儲存成功傳回true，失敗時傳回false。
func.saveTable{Table,String,UserData,1,
function (t, filename,directory)
	-- local path= system.pathForFile("", directory or system.DocumentsDirectory).."/"..filename
	local path= system.pathForFile(filename, directory or system.DocumentsDirectory)
	if(path) then
		local file = io.open(path, "w")
		if file then
			local contents = json.encode(t)
			if(contents) then
				file:write( contents )
			end
			io.close( file )
			return true
		else
			return false
		end
	else
		-- print("WARNING: Calling JsonMngr:saveTable() Failed. Cannot find file:"..filename)
		warn(JsonMngr,"saveTable","Cannot find file:"..filename)
		return false
	end
end,Boolean}

--載入table
--filename 載入檔案路徑
--directory 檔案目錄 預設值 system.DocumentsDirectory
--傳回值： (table) 被載入的table，載入失敗時傳回nil。
func.loadTable{String,UserData,1,
function (filename,directory)
    local path= system.pathForFile(filename, directory or system.DocumentsDirectory)
    if(path) then
		local contents = ""
		local myTable = {}
		local file = io.open( path, "r" )
		if file then
			 -- read all contents of file into a string
			 local contents = file:read( "*a" )
			 if(contents~=nil and contents~="")then
				myTable = json.decode(contents);
			 end
			 io.close( file )
			 return myTable
		end
	else
		-- print("WARNING: Calling JsonMngr:loadTable() Failed. Cannot find file:"..filename)
		warn(JsonMngr,"loadTable","Cannot find file:"..filename)
	end
    return nil
end}
--傳回類別
return endClass()

