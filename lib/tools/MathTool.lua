require("lib.OOP")
--## package path
packagePath("lib.tools.MathTool")

--## package requirements
local Point=require("lib.geometric.Point")
--## class: MathTool 　
--MathTool 類別提供多種 數學上的工具方法，MathTool跟 lua的 math不同，MathTool所提供的是比較進階且特定的方法。
--此處提供的方法主要與角度運算有關。座標操作方面的工具已經被移轉給 Point類別，請使用 Point物 件進行座標的轉換與移動操作。
local MathTool=abstract(class("MathTool"))

----<類別 私有屬性>----
local piDividedBy180=math.pi/180
local xLen=0
local yLen=0

----<類別 私有方法>----
local function getXYLen(x1,y1,x2,y2)
	xLen=x2-x1
	yLen=y2-y1
end

--## static public property
--弳度轉換成角度
func.radiansToAngle{Number,
function (radians)
	return radians/piDividedBy180
end,Number}
--角度轉換成弳度
func.angleToRadians{Number,
function (angle)
	return angle*piDividedBy180
end,Number}
--取得兩點距離
--建議使用Point物件上的distance()方法
func.getDistance{Number,Number,Number,Number,
function (x1,y1,x2,y2)
	getXYLen(x1,y1,x2,y2)
	return math.sqrt(math.pow(xLen,2)+math.pow(yLen,2))
end,Number}

--兩旋轉角度相減
--傳回必定小於180度的最小夾角
func.subtractRotation{Number,Number,
function (rot1,rot2)
	local subRot=(rot1)-rot2
	local absSub=math.abs(subRot)
	local one=subRot/absSub
	if(absSub>180) then
		subRot=(360-absSub)*one*-1
	end
	return subRot
end,Number}
--傳回類別
return endClass()

