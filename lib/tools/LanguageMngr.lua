require("lib.OOP")
--## package path
packagePath("lib.tools.LanguageMngr")
--## package requirements
local AppInfo=require("lib.qll.appInfo.AppInfo")

--## class: LanguageMngr
--如果你有一個碗糕，它有很多語言的版本，把他們通通給我，我會看場合見人說人話見鬼說鬼話。
--*目前不提供從外部語言包載入的功能。以後說不定。
local LanguageMngr=abstract(class("LanguageMngr"))--建立新類別 LanguageMngr
--## static private property
local langList--語系名稱列表
local langIndexDict={}--語系名稱轉換為語系索引的對照表
local defaultLangIndex=1
local inited=false
--## static public property

--## static private method
local function getLangIndex(language)
	return langIndexDict[language] or defaultLangIndex
end
--檢驗是否初始化
local function checkInit(funcName)
	if(not inited) then
		print("ERROR: Calling '"..funcName.."' failed. 'LanguageMngr:init()' required before calling this function.")
	end
end
--## static public method
--初始化語言設定
--指定這個 App所使用的預設語言與其他語言。
--defaultLang 預設語言名稱
--... (String) 其他語言名稱
func.init{String,
function (defaultLang,...)
	langList={defaultLang,...}
	if(not inited) then
		inited=true
		--建立對照索引
		for langIndex, language in pairs(langList) do
			langIndexDict[language]=langIndex
		end
	else
		print("ERROR: Calling 'LanguageMngr:init()' failed. Multiple init. LanguageMngr can only be inited once.")
	end
end}

--自動偵測語系並取得對應的內容
--... (Any) 每個語系的內容。
function LanguageMngr:get(...)
	checkInit("LanguageMngr:get()")
	local contentList={...}
	return contentList[getLangIndex(AppInfo.language)]
end

--取得指定語系對應的內容
--language 指定的語系
--... (Any) 每個語系的內容。
func.getByLang{String,
function(language,...)
	checkInit("LanguageMngr:getByLang()")
	local langIndex=getLangIndex(language)
	local contentList={...}
	return contentList[langIndex]
end}
--defaultLang 預設語言
function set:defaultLang(language)
	defaultLangIndex=getLangIndex(language)
end
function get:defaultLang()
	return langList[defaultLangIndex]
end

--傳回類別
return endClass()

