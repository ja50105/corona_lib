require("lib.OOP")
--## package path
packagePath("lib.tools.SizeMngr")
--## package requirements
local RectArea=require("lib.geometric.RectArea")
--## class: SizeMngr
--SizeMngr負責幫忙修改各種東西的尺寸大小，通常是顯示物件(OOP Library 和 Corona SDK的 DisplayObject皆可)。
--SizeMngr是基於物件的 width, height, xScale, yScale屬性運作。aWidth和 aHeight屬性不在其考量範圍。
local SizeMngr=abstract(class("SizeMngr"))
--## static private property
--## static public property
const.FIT_BOTH="fitBoth"
const.FIT_WIDTH="fitWidth"
const.FIT_HEIGHT="fitHeight"
const.H_ALIGN_CENTER="centerX"
const.H_ALIGN_LEFT="left"
const.H_ALIGN_RIGHT="right"
const.V_ALIGN_CENTER="centerY"
const.V_ALIGN_TOP="top"
const.V_ALIGN_BOTTOM="bottom"
--## static private method

--## static public method
--會傳回一個代表螢幕方框尺寸與位置資訊的 RectArea 物件
function SizeMngr:getScreenRectArea()
	local width=display.contentWidth-display.screenOriginX*2
	local height=display.contentHeight-display.screenOriginY*2
	return RectArea:new(display.screenOriginX,display.screenOriginY,width,height)
end

--將顯示物件縮放以符合區域大小
--rectArea 指定的區域
--displayObj 要變更大小的顯示物件
--lockScale  鎖定寬高比例，預設為true
--fitMode 設置以寬或高為縮放基準。可為三個值：SizeMngr.FIT_BOTH, SizeMngr.FIT_WIDTH, SizeMngr.FIT_HEIGHT，預設為 SizeMngr.FIT_BOTH
func.fitArea{RectArea,Table,Boolean,String,2,
function (rectArea,displayObj,lockScale,fitMode)
	if(lockScale==nil) then
		lockScale=true
	end
	fitMode=fitMode or SizeMngr.FIT_BOTH
	local xScale=rectArea.width/displayObj.width
	local yScale=rectArea.height/displayObj.height
	if(lockScale) then
		local scale={}
		scale[SizeMngr.FIT_BOTH]=math.min(xScale,yScale)
		scale[SizeMngr.FIT_WIDTH]=xScale
		scale[SizeMngr.FIT_HEIGHT]=yScale
		displayObj.xScale=scale[fitMode]
		displayObj.yScale=scale[fitMode]
	else
		if(fitMode==SizeMngr.FIT_WIDTH or fitMode==SizeMngr.FIT_BOTH) then
			displayObj.xScale=xScale	
		end
		if(fitMode==SizeMngr.FIT_HEIGHT or fitMode==SizeMngr.FIT_BOTH) then
			displayObj.yScale=yScale
		end
	end	
end
}
--將顯示物件對齊區域，對齊時不會更動到物件的 anchorX,anchorY屬性
--rectArea 指定的區域
--displayObj (object displayObject) 要對齊的顯示物件
--hAlign 水平對齊方式，可為三個值：SizeMngr.H_ALIGN_CENTER, SizeMngr.H_ALIGN_LEFT, SizeMngr.H_ALIGN_RIGHT。預設值：SizeMngr.H_ALIGN_CENTER
--vAlign 垂直對齊方式，可為三個值：SizeMngr.V_ALIGN_CENTER, SizeMngr.V_ALIGN_TOP, SizeMngr.V_ALIGN_BOTTOM。預設值：SizeMngr.V_ALIGN_CENTER
func.align{RectArea,Table,String,String,2,
function (rectArea,displayObj,hAlign,vAlign)
	hAlign=hAlign or SizeMngr.H_ALIGN_CENTER
	vAlign=vAlign or SizeMngr.V_ALIGN_CENTER
	local widthDiff=rectArea.width-displayObj.width
	local heightDiff=rectArea.height-displayObj.height
	local anchorXOffset=displayObj.width*displayObj.anchorX
	local anchorYOffset=displayObj.height*displayObj.anchorY
	local alignXRatio={centerX=.5,left=0,right=1}
	local alignYRatio={centerY=.5,top=0,right=1}
	displayObj.x=rectArea.left+widthDiff*alignXRatio[hAlign]+anchorXOffset
	displayObj.y=rectArea.top+heightDiff*alignYRatio[vAlign]+anchorYOffset
end
}

--傳回類別
return endClass()

