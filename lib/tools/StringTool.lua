--## package path
packagePath("lib.tools.StringTool")

--## package requirements
local OOP=require("lib.OOP")

--## class: StringTool
--如果字串(String)在虐待你，那就讓我來教訓它。我可以幫你把它碎屍萬段，再照順序還給你一個Array。
local StringTool=abstract(class("StringTool"))--建立新類別
--## static private property

--## static public property

--## static private method

--## static public method
--將字串以指定的分隔字元，切成一個陣列
func.split{String,String,
function (text,delimiter)
	local array={}
	local pos = 1
	if string.find("", delimiter, 1) then -- this would result in endless loops
		error("delimiter matches empty string!")
	end
	while 1 do
		local first, last = string.find(text, delimiter, pos)
		if first then -- found?
			table.insert(array, string.sub(text, pos, first-1))
			pos = last+1
		else
			table.insert(array, string.sub(text, pos))
			break
		end
	end
	return array
end,Table}
--(剩下能做的好像都被Corona做完了：join=table.concat,slice=string.sub ....)
--傳回類別
return endClass()

